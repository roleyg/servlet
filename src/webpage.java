import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.*;
import java.util.Scanner;

@WebServlet(name = "webpage", urlPatterns = {"/webpage"})
public class webpage extends HttpServlet {
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Given an investment amount and an annual interest rate, this program \n" +
                "will calculate the future value of the investment for a period of\n" +
                "ten years. ");

        Scanner input = new Scanner(System.in);
        final int NUMBER_OF_YEARS = 10;

        // Prompts user to enter investment amount and annual interest rate
        System.out.print("\nEnter the amount invested: ");
        double amount = input.nextDouble();
        System.out.print("Enter annual interest rate (0 - 100): ");
        double annualInterestRate = input.nextDouble();

        // Calculate the monthly interest rate
        double monthlyInterestRate = annualInterestRate / 1200;

        // Displays a table for years 1 - 10
        System.out.println("Years     Future Value");
        for (int years = 1; years <= NUMBER_OF_YEARS; years++) {
            System.out.printf("%-10d", years);
            System.out.printf("%11.2f\n",
                    futureValue(amount, monthlyInterestRate, years));
        }
    }

    public static double futureValue(
            double investmentAmount, double monthlyInterestRate, int years) {
        return investmentAmount * Math.pow(1 + monthlyInterestRate, years * 12);
    }
}

    protected void doGet (HttpServletRequest request, @NotNull HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Are you sure you know what you are doing?");
    }
}
